<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    public function createRequest(Request $request)
    {
        //validate request first
        $this->validate($request, [
            'fullname' => ['required', 'string', 'max:50'],
            'phone' => 'required|numeric|regex:/^[789]\d{9}$/',
            'email' => ['required', 'string', 'email', 'max:255'],
            'amount' => 'required',
        ]);

        if($request->amount > 9)
        {
            //start of payment gateway
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_HTTPHEADER,
                        array("X-Api-Key: ".config('services.instamojo.api_key'),
                            "X-Auth-Token: ".config('services.instamojo.auth_token')));

            $payload = Array(
                'purpose' => 'Payment gateway tutorial',
                'amount' => $request->amount,
                'phone' => $request->phone,
                'buyer_name' => $request->fullname,
                'redirect_url' => url('/redirect'),
                'send_email' => false,
                'webhook' => 'http://www.example.com/webhook/',
                'send_sms' => false,
                'email' => $request->email,
                'allow_repeated_payments' => false
            );

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
            $response = curl_exec($ch);
            curl_close($ch);

            $data = json_decode($response, true);

            if($data != null && $data['success'] == true) {
                return redirect($data['payment_request']['longurl']);
            } else {
                return redirect()->back()->with('error', 'Payment failed. Please try again later.');
            }
        }else{
            return redirect()->back()->with('error', 'Amount cannot be less than INR 10.00');
        }
    }
    
    public function redirect(Request $request)
    {
        $api = new \Instamojo\Instamojo(
            config('services.instamojo.api_key'),
            config('services.instamojo.auth_token'),
            config('services.instamojo.url')
        );

        $response = $api->paymentRequestStatus(request('payment_request_id'));

        if($response)
        {
            //create Payment
            $payment = new Payment;
            $payment->payment_id = $response['payments'][0]['payment_id'];
            $payment->payment_request_id = $response['id'];
            $payment->payment_status = $response['payments'][0]['status'];
            
            $payment->fullname = $response['buyer_name'];
            $payment->phone = $response['phone'];
            $payment->email = $response['email'];
            $payment->amount = $response['amount'];
            $payment->currency = $response['payments'][0]['currency'];
            $payment->status = $response['status'];
            
            //save in db
            $payment->save();
        }

        return redirect('/')->with('success', 'Payment Successful.');
    }
}
