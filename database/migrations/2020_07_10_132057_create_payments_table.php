<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id')->unique()->nullable(false);
            $table->string('payment_id',150)->nullable(false);
            $table->string('payment_request_id',150)->nullable(false);
            $table->string('payment_status',150)->nullable(false);
            $table->string('fullname',50)->nullable(false);
            $table->string('phone',100)->nullable(false);
            $table->string('email')->nullable(false);
            $table->double('amount')->nullable(false);
            $table->string('currency',10)->nullable(false);
            $table->string('status',150)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
