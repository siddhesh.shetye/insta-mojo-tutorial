@if(count($errors) > 0)
@foreach($errors->all() as $error)
<div class="col-md-12">
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>{{$error}}</strong>
    </div>
</div>
@endforeach
@endif

@if(session('success'))
<div class="col-md-12">
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{session('success')}}
    </div>
</div>
@endif

@if(session('error'))
<div class="col-md-12">
    <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>Alert!</strong> {{session('error')}}
    </div>
</div>
@endif