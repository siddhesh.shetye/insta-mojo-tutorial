<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>

<body>
    <div class="main-panel">
        <div class="container">
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body align-self-center">
                            <h4 class="card-title">Insta Mojo Tutorial</h4>
                            <div class="media">
                                <i class="fa fa-hashtag icon-size icon-color d-flex align-self-center mr-3"
                                    aria-hidden="true"></i>
                                <div class="media-body">
                                    <p class="card-text">This tutorial is for the purpose of showing insta mojo test
                                        payment gateway using laravel.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @include('messages')

                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Insta Mojo Tutorial</h4>

                            {!! Form::open(['action' => ['PaymentController@createRequest'] , 'method' => 'POST',
                            'class' => 'donationform', 'enctype' => 'multipart/form-data']) !!}
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="fullname">Fullname <b>*</b></label>
                                <input id="fullname" type="text" placeholder="Enter full name" class="form-control"
                                    name="fullname" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone number <b>*</b></label>
                                <input id="phone" type="text" placeholder="Enter phone number" class="form-control"
                                    name="phone" required minlength="10" maxlength="10" pattern="^[789]\d{9}$"
                                    data-toggle="tooltip" data-placement="right"
                                    title="Number must be of 10 digits and must start with 7, 8 or 9.">
                            </div>

                            <div class="form-group">
                                <label for="email">Email <b>*</b></label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" placeholder="Enter email" value="{{ old('email') }}" required
                                    autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="amount">Payment Amount <b>*</b></label>
                                <input id="amount" type="number" min="10" placeholder="Enter donation amount"
                                    class="form-control" name="amount" required>
                            </div>

                            <button type="submit" class="btn btn-primary mr-2">Submit</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JS here -->
    <script type="text/javascript" src="{{ asset('js/jquery-1.12.4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 4000);
    </script>
</body>

</html>